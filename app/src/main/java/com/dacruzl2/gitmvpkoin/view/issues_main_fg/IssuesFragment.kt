package com.dacruzl2.gitmvpkoin.view.issues_main_fg


import android.content.Context
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

import com.dacruzl2.gitmvpkoin.R
import com.dacruzl2.gitmvpkoin.adapters.IssuesAdapter
import com.dacruzl2.gitmvpkoin.data.Root
import kotlinx.android.synthetic.main.fragment_issues.*

import org.koin.android.ext.android.inject
import org.koin.core.parameter.parametersOf

class IssuesFragment : Fragment(), IssuesContract.View {
    override fun showErrorMessage() {
    }

    val presenter: IssuesContract.Presenter by inject { parametersOf(this)}

    val issueList: MutableList<Root> by lazy { mutableListOf<Root>() }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        presenter.attachView(this)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        val view = inflater.inflate(R.layout.fragment_issues, container, false)

        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)


        with(rv_issuelist) {
            adapter = IssuesAdapter(activity!! as Context, issueList)
            layoutManager = LinearLayoutManager(activity, RecyclerView.VERTICAL, false)
            setHasFixedSize(true)
        }
    }

    override fun setDataToRecyclerView(list: List<Root>) {
        issueList.addAll(list)
        rv_issuelist.adapter?.notifyDataSetChanged()
    }

    override fun showProgress() {
    }

    override fun hideProgress() {
    }

}
