package com.dacruzl2.gitmvpkoin.view.issues_main_fg

import com.dacruzl2.gitmvpkoin.base.BasePresenter
import com.dacruzl2.gitmvpkoin.data.source.GithubRepository
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

class IssuesPresenter(val repository: GithubRepository) : BasePresenter<IssuesContract.View>(),
    IssuesContract.Presenter {

    init { requestDataFromServer() }

    override fun requestDataFromServer() {
        GlobalScope.launch {
            repository.getIssues {
                view?.setDataToRecyclerView(it)
            }
        }
    }
}

