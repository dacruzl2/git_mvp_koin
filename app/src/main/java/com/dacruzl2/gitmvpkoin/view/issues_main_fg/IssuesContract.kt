package com.dacruzl2.gitmvpkoin.view.issues_main_fg


import com.dacruzl2.gitmvpkoin.base.MvpPresenter
import com.dacruzl2.gitmvpkoin.base.MvpView
import com.dacruzl2.gitmvpkoin.data.Root

interface IssuesContract {

    interface View : MvpView {

        fun setDataToRecyclerView(list: List<Root>)

        fun showProgress()

        fun hideProgress()

        fun showErrorMessage()
    }

    interface Presenter : MvpPresenter<View> {

        fun requestDataFromServer()

    }
}