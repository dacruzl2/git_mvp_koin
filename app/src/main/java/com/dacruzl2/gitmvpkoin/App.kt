package com.dacruzl2.gitmvpkoin

import android.app.Application
import android.content.Context
import android.net.ConnectivityManager
import com.dacruzl2.gitmvpkoin.di.appModule
import com.dacruzl2.gitmvpkoin.di.db_module
import com.dacruzl2.gitmvpkoin.di.networkModule
import com.facebook.stetho.Stetho
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin
import org.koin.core.logger.Level

class App: Application() {


    companion object {

        lateinit var instance: App

        fun appContext(): Context = instance.applicationContext

        fun isNetworkAvailable(): Boolean {
            val cm = instance.applicationContext
                .getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
            return cm.activeNetworkInfo?.isConnected ?: false
        }
    }

    override fun onCreate() {
        super.onCreate()

        instance = this

        if (BuildConfig.DEBUG) {
            Stetho.initializeWithDefaults(this@App)
        }

        startKoin{
            androidLogger(Level.DEBUG)
            androidContext(this@App)
            modules(listOf(appModule, networkModule, db_module))
        }
    }
}