package com.dacruzl2.gitmvpkoin.di

import android.util.Log
import androidx.room.Room
import com.dacruzl2.gitdesafio.util.URL_BASE

import com.dacruzl2.gitmvpkoin.data.source.GithubDataSource
import com.dacruzl2.gitmvpkoin.data.source.GithubRepository
import com.dacruzl2.gitmvpkoin.data.source.local.GithubDatabase
import com.dacruzl2.gitmvpkoin.data.source.remote.services.GithubService
import com.dacruzl2.gitmvpkoin.view.issues_main_fg.IssuesContract
import com.dacruzl2.gitmvpkoin.view.issues_main_fg.IssuesPresenter

import com.jakewharton.retrofit2.adapter.kotlin.coroutines.CoroutineCallAdapterFactory
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor

import org.koin.android.ext.koin.androidApplication
import org.koin.dsl.module

import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

val appModule = module {

    factory<IssuesContract.Presenter> { (_: IssuesContract.View) ->
        IssuesPresenter(get())
    }

    factory<GithubDataSource> { GithubRepository(get(), get()) }

    single { GithubRepository(get(), get()) }

}

val networkModule = module {
    factory<Interceptor> {
        HttpLoggingInterceptor(HttpLoggingInterceptor.Logger { Log.d("API", it) })
            .setLevel(HttpLoggingInterceptor.Level.BODY)
    }

    factory { OkHttpClient.Builder().addInterceptor(get()).build() }

    single {
        Retrofit.Builder()
            .client(get())
            .baseUrl(URL_BASE)
            .addConverterFactory(GsonConverterFactory.create())
            .addCallAdapterFactory(CoroutineCallAdapterFactory())
            .build()
    }

    factory { get<Retrofit>().create(GithubService::class.java) }
}

val db_module = module {

    single {
        Room.databaseBuilder(androidApplication(), GithubDatabase::class.java, "github-db")
            .build()
    }
    single { get<GithubDatabase>().githubDAO() }
}

