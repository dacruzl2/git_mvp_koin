package com.dacruzl2.gitmvpkoin.data.source

import com.dacruzl2.gitmvpkoin.data.Root

interface GithubDataSource {

    suspend fun getIssues(onSuccess: (issues: List<Root>) -> Unit)

    suspend fun saveIssues(issues: List<Root>)
}

