package com.dacruzl2.gitmvpkoin.data.source.remote

import com.dacruzl2.gitmvpkoin.data.Root
import com.dacruzl2.gitmvpkoin.data.source.GithubDataSource
import com.dacruzl2.gitmvpkoin.data.source.remote.services.GithubService
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import java.lang.Exception

class IssuesRemoteDataSource(private val remoteSource: GithubService) : GithubDataSource {

    override suspend fun getIssues(onSuccess: (repos: List<Root>) -> Unit) = withContext(Dispatchers.IO) {
        try {
            val request = remoteSource.getIssues().await()
            if (request.isSuccessful) {
                withContext(Dispatchers.Main) {
                    val issuesList = request.body()!!
                    onSuccess(issuesList)
                }

            } else {}

        } catch (t: Exception) {}
    }

    override suspend fun saveIssues(issues: List<Root>) {

    }
}
