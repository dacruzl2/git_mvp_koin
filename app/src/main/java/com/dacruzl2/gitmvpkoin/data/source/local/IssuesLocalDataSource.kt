package com.dacruzl2.gitmvpkoin.data.source.local

import com.dacruzl2.gitmvpkoin.data.Root
import com.dacruzl2.gitmvpkoin.data.source.GithubDataSource
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class IssuesLocalDataSource private constructor(val dao: GithubDao) : GithubDataSource {


    override suspend fun getIssues(onSuccess: (issues: List<Root>) -> Unit) = withContext(Dispatchers.IO) {

        val issues = dao.getIssues()
        if (issues.isNotEmpty()) {
            onSuccess(dao.getIssues())
        } else {
            //Result.Error(LocalDataNotFoundException())
        }
    }

    override suspend fun saveIssues(issues: List<Root>)= withContext(Dispatchers.IO) {
        dao.saveIssues(issues)
    }

}