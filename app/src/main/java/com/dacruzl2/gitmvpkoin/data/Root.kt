package com.dacruzl2.gitmvpkoin.data

import android.os.Parcelable
import androidx.room.*
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Entity
@Parcelize
data class Root constructor(
    @Embedded
    @SerializedName("assignee") var assignee: Assignee?,
    @SerializedName("assignees") var assignees: List<Assignee>? = emptyList(),
    @SerializedName("author_association") var authorAssociation: String?,
    @SerializedName("body") var body: String?,
    @SerializedName("closed_at") var closedAt: Int?,
    @SerializedName("comments") var comments: Int?,
    @SerializedName("comments_url") var commentsUrl: String?,
    @SerializedName("created_at") var createdAt: String?,
    @ColumnInfo(name = "events_root") @SerializedName("events_url") var eventsUrl: String?,
    @ColumnInfo(name = "html_root") @SerializedName("html_url") var htmlUrl: String?,
    @PrimaryKey(autoGenerate = true)
    @SerializedName("id") var id: Int?,
    @SerializedName("labels") var labels: List<Label>? = emptyList(),
    @SerializedName("labels_url") var labelsUrl: String?,
    @SerializedName("locked") var locked: Boolean?,
    @SerializedName("number") var number: Int?,
    @Embedded
    @SerializedName("pull_request") var pullRequest: PullRequest?,
    @SerializedName("repository_url") var repositoryUrl: String?,
    @SerializedName("state") var state: String?,
    @SerializedName("title") var title: String?,
    @SerializedName("updated_at") var updatedAt: String?,
    @ColumnInfo(name = "url_root") @SerializedName("url") var url: String?,
    @ColumnInfo(name = "node_id_root") @SerializedName("node_id") var nodeId: String?,
    @Embedded
    @SerializedName("user") var user: User?
) : Parcelable {
    constructor(assignee: Assignee, pullRequest: PullRequest, user: User) : this(
        assignee, emptyList(), "", "", 0, 0,
        "", "", "", "", 0, emptyList(), "", false, 0,
        pullRequest, "", "", "", "", "", "", user
    )
}

@Entity
@Parcelize
data class PullRequest(
    @ColumnInfo(name = "diff_url_pull") @SerializedName("diff_url") var diffUrl: String,
    @ColumnInfo(name = "html_url_pull") @SerializedName("html_url") var htmlUrl: String,
    @ColumnInfo(name = "patch_url_pull") @SerializedName("patch_url") var patchUrl: String,
    @PrimaryKey
    @ColumnInfo(name = "url_pull") @SerializedName("url") var url: String
) : Parcelable {
    @Ignore constructor() : this("", "", "", "")
}

@Entity
@Parcelize
data class User(
    @ColumnInfo(name = "avatar_user") @SerializedName("avatar_url") var avatarUrl: String,
    @ColumnInfo(name = "events_user") @SerializedName("events_url") var eventsUrl: String,
    @ColumnInfo(name = "folowers_user") @SerializedName("followers_url") var followersUrl: String,
    @ColumnInfo(name = "following_url_user") @SerializedName("following_url") var followingUrl: String,
    @ColumnInfo(name = "gists_user") @SerializedName("gists_url") var gistsUrl: String,
    @ColumnInfo(name = "gravatar_id_user") @SerializedName("gravatar_id") var gravatarId: String,
    @ColumnInfo(name = "html_url_user") @SerializedName("html_url") var htmlUrl: String,
    @PrimaryKey
    @ColumnInfo(name = "id_user") @SerializedName("id") var id: Int,
    @ColumnInfo(name = "login_user") @SerializedName("login") var login: String,
    @ColumnInfo(name = "node_id_user") @SerializedName("node_id") var nodeId: String,
    @ColumnInfo(name = "org_user") @SerializedName("organizations_url") var organizationsUrl: String,
    @ColumnInfo(name = "received_user") @SerializedName("received_events_url") var receivedEventsUrl: String,
    @ColumnInfo(name = "repos_url_user") @SerializedName("repos_url") var reposUrl: String,
    @ColumnInfo(name = "site_admin_user") @SerializedName("site_admin") var siteAdmin: Boolean,
    @ColumnInfo(name = "starred_user") @SerializedName("starred_url") var starredUrl: String,
    @ColumnInfo(name = "subscription_user") @SerializedName("subscriptions_url") var subscriptionsUrl: String,
    @ColumnInfo(name = "type_user") @SerializedName("type") var type: String,
    @ColumnInfo(name = "url_user") @SerializedName("url") var url: String
) : Parcelable {
    constructor() : this(
        "", "", "", "", "", "", "",
        0, "", "", "", "", "", false, "",
        "", "", ""
    )
}

@Entity
@Parcelize
data class Label(
    @ColumnInfo(name = "color_label") @SerializedName("color") var color: String,
    @ColumnInfo(name = "default_label") @SerializedName("default") var default: Boolean,
    @PrimaryKey
    @ColumnInfo(name = "id_label") @SerializedName("id") var id: Int,
    @ColumnInfo(name = "name_label") @SerializedName("name") var name: String,
    @ColumnInfo(name = "node_id_label") @SerializedName("node_id") var nodeId: String,
    @ColumnInfo(name = "url_label") @SerializedName("url") var url: String
) : Parcelable {
    constructor() : this("", false, 0, "", "", "")
}

@Entity
@Parcelize
data class Assignee(
    @ColumnInfo(name = "avatar_url_assignee") @SerializedName("avatar_url") var avatarUrl: String?,
    @ColumnInfo(name = "event_assignee") @SerializedName("events_url") var eventsUrl: String,
    @ColumnInfo(name = "followers_assignee") @SerializedName("followers_url") var followersUrl: String,
    @ColumnInfo(name = "following_assignee") @SerializedName("following_url") var followingUrl: String,
    @ColumnInfo(name = "gists_assignee") @SerializedName("gists_url") var gistsUrl: String,
    @ColumnInfo(name = "gravatar_assignee") @SerializedName("gravatar_id") var gravatarId: String,
    @ColumnInfo(name = "html_assignee") @SerializedName("html_url") var htmlUrl: String,
    @PrimaryKey
    @ColumnInfo(name = "id_assignee") @SerializedName("id") var id: Int,
    @ColumnInfo(name = "login_assignee") @SerializedName("login") var login: String,
    @ColumnInfo(name = "node_id_assignee") @SerializedName("node_id") var nodeId: String,
    @ColumnInfo(name = "org_assignee") @SerializedName("organizations_url") var organizationsUrl: String,
    @ColumnInfo(name = "received_assignee") @SerializedName("received_events_url") var receivedEventsUrl: String,
    @ColumnInfo(name = "repos_assignee") @SerializedName("repos_url") var reposUrl: String,
    @ColumnInfo(name = "site_admin_assignee") @SerializedName("site_admin") var siteAdmin: Boolean,
    @ColumnInfo(name = "starred_assignee") @SerializedName("starred_url") var starredUrl: String,
    @ColumnInfo(name = "subscript_assignee") @SerializedName("subscriptions_url") var subscriptionsUrl: String,
    @ColumnInfo(name = "type_assignee") @SerializedName("type") var type: String,
    @ColumnInfo(name = "url_assignee") @SerializedName("url") var url: String
) : Parcelable {
    constructor() : this("", "", "", "", "",
        "", "", 0, "", "", "", "",
        "", false, "", "", "", "")
}