package com.dacruzl2.gitmvpkoin.data.source.local

import com.dacruzl2.gitmvpkoin.data.Assignee
import com.dacruzl2.gitmvpkoin.data.Label
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import androidx.room.TypeConverter


class TypeConverter {
    companion object{
        @TypeConverter @JvmStatic // note this annotation
        fun fromOptionValuesList(optionValues: List<Assignee>?): String? {
            if (optionValues == null) {
                return null
            }
            val gson = Gson()
            val type = object : TypeToken<List<Assignee>>() {

            }.type
            return gson.toJson(optionValues, type)
        }

        @TypeConverter @JvmStatic // note this annotation
        fun toOptionValuesList(optionValuesString: String?): List<Assignee>? {
            if (optionValuesString == null) {
                return null
            }
            val gson = Gson()
            val type = object : TypeToken<List<Assignee>>() {

            }.type
            return gson.fromJson<List<Assignee>>(optionValuesString, type)
        }

        @TypeConverter @JvmStatic
        fun fromOptionValueList(optionValue: List<Label>?): String? {
            if (optionValue == null) {
                return null
            }
            val gson = Gson()
            val type = object : TypeToken<List<Label>>() {

            }.type
            return gson.toJson(optionValue, type)
        }

        @TypeConverter @JvmStatic // note this annotation
        fun toOptionValueList(optionValueString: String?): List<Label>? {
            if (optionValueString == null) {
                return null
            }
            val gson = Gson()
            val type = object : TypeToken<List<Label>>() {

            }.type
            return gson.fromJson<List<Label>>(optionValueString, type)
        }
    }
}
