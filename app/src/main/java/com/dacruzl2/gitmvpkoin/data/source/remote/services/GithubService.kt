package com.dacruzl2.gitmvpkoin.data.source.remote.services

import com.dacruzl2.gitmvpkoin.data.Root
import kotlinx.coroutines.Deferred
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Path

interface GithubService {

    @GET("/repos/{owner}/{repo}/issues")
    fun getIssues(
        @Path("owner") owner: String = "JetBrains",
        @Path("repo") repo: String = "Kotlin"
    ): Deferred<Response<List<Root>>>
}