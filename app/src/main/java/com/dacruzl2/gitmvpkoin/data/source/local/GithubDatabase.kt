package com.dacruzl2.gitmvpkoin.data.source.local

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.dacruzl2.gitmvpkoin.data.*

@Database(entities = [Root::class, PullRequest::class,
                      User::class, Label::class, Assignee::class],
    version = 1, exportSchema = false)
@TypeConverters(TypeConverter::class)
abstract class GithubDatabase : RoomDatabase() {
    abstract fun githubDAO(): GithubDao
}