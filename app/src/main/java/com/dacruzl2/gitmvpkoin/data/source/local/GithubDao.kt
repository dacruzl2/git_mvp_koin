package com.dacruzl2.gitmvpkoin.data.source.local

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.dacruzl2.gitmvpkoin.data.Root

@Dao
interface GithubDao {

    @Query("SELECT * FROM Root")
    fun getIssues(): List<Root>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun saveIssues(issues: List<Root>)


    @Query("DELETE FROM Root")
    fun deleteIssues()

}