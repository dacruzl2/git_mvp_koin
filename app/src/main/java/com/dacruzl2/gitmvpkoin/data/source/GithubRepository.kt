package com.dacruzl2.gitmvpkoin.data.source

import com.dacruzl2.gitmvpkoin.data.Root

class GithubRepository(
    private val remoteDataSource: GithubDataSource,
    private val localDataSource: GithubDataSource
) : GithubDataSource {

    internal var cachedItemList: List<Root>? = null

    /**
     * Marks the cache as invalid, to force an update the next time data is requested. This variable
     * has package local visibility so it can be accessed from tests.
     */

    internal var cacheIsDirty = false


    override suspend fun getIssues(onSuccess: (issues: List<Root>) -> Unit) {

    }

    override suspend fun saveIssues(issues: List<Root>) {

    }


}

/* withContext(Dispatchers.IO) {
           try {
               val request = remoteDataSource.getIssues().await()

               if (request.isSuccessful) {

                   withContext(Dispatchers.Main) {

                       val issuesList = request.body()!!

                       onSuccess(issuesList)

                       withContext(Dispatchers.IO){
                           Log.d("DB", "initSaveDB")

                           localDataSource.saveIssues(issuesList)

                           Log.d("DB", "DBsaved: $issuesList")

                       }
                   }
               } else {

               }
           } catch (t: Exception) {

           }
       }*/