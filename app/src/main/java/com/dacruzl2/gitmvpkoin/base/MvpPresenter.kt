package com.dacruzl2.gitmvpkoin.base



interface MvpPresenter<V : MvpView> {

    val isViewAttached: Boolean

    fun attachView(mvpView: V)

    fun detachView()
}